### Hoe start ik deze app?
Note: zo werkt het op mijn Mac: misschien op Windows anders!!

1. Ga in de terminal naar de map waar deze code staat. Dus in de map `django-weather`.
2. (dit hoeft alleen maar de eerste keer!!) Maak een virtual environment aan: `python -m venv .venv`. De virtual environment komt dan in een geheime map `.venv` terecht.
3. Activeer de environment: `.\.venv\Scripts\activate`.
4. Installeer de requirements: `pip install -r requirements.txt`: alle requirements uit deze file worden dan geinstalleerd.
5. Je kunt de Django website starten door het commando `python manage.py runserver`; dan is de webpagina bereikbaar op localhost:8000. 



Schematische weergave van een Django applicatie door top-student Jeroen:
![alt text](plaatJeroenR.jpeg?raw=True)
