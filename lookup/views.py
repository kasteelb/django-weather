from django.shortcuts import render
import json
import requests
from lookup.helpers import set_description_and_color, url_for_plaats, url_for_zipcode, set_overige_waarden, set_overige_waarden_nl


def buitenland(request):
	if request.method == "POST":
		zipcode = request.POST['zipcode']
	else:
		zipcode = '89129'

	api_request = requests.get(url_for_zipcode(zipcode))
	api_content = json.loads(api_request.content)
	api_empty = len(api_content) == 0

	if not api_empty:
		category_description, category_color = set_description_and_color(api_content)
		category_name, aqi, area = set_overige_waarden(api_content)

	else:
		category_description = 'No description; there was some error'
		category_color = 'No color, there was some error'

	return render(request, 'home.html', {
		'api_content': api_content,
		'api_empty': api_empty,
		'category_description': category_description,
		'category_color': category_color,
		'category_name': category_name,
		'aqi': aqi,
		'area': area
		})


def nederland(request):
	if request.method == "POST":
		zipcode = request.POST['city']
	else:
		zipcode = 'leidschendam'

	api_request = requests.get(url_for_plaats(zipcode))
	api_content = json.loads(api_request.content)
	api_empty = len(api_content) == 0

	if not api_empty:
		city, temp, samenv, windr, zicht, verw, sunder, afbeelding = set_overige_waarden_nl(api_content)

	else:
		city = 'City not found'
		temp = 'No temperature; there was some error'

	return render(request, 'nederland.html', {
		'api_content': api_content,
		'api_empty': api_empty,
		'city': city,
		'temp': temp,
		'samenv': samenv,
		'windr': windr,
		'zicht': zicht,
		'verw': verw,
		'sunder': sunder,
		'afbeelding': afbeelding,
		})


def about(request):
	return render(request, 'about.html', {})


def help(request):
	return render(request, 'help.html', {})


