def url_for_zipcode(zipcode: str):
    return "http://www.airnowapi.org/aq/observation/zipCode/current/?format=application/json&zipCode=" + \
           str(zipcode) + \
           "&distance=5&API_KEY=96A38DFD-5C56-4740-AD99-E38C0C855A1B"


def url_for_plaats(plaatsnaam: str):
    return "https://weerlive.nl/api/json-data-10min.php?key=3a639934ee&locatie=" + \
           str(plaatsnaam)


def set_description_and_color(air_now_api_result):
    if air_now_api_result[0]['Category']['Name'] == "Good":
        description = "(0 -50) Air quality is considered satisfactory, and air pollution poses little or no risk."
        category_color = "good"
    elif air_now_api_result[0]['Category']['Name'] == "Moderate":
        description = "(51-100) Air quality is acceptable; " \
                      "however, for some pollutants there may be a moderate health concern for " \
                      "a very small number of people who are unusually sensitive to air pollution."
        category_color = "moderate"
    elif air_now_api_result[0]['Category']['Name'] == "Unhealthy for Sensitive Groups":
        description = "(101 - 150) Although general public is not likely to be affected at this AQI range, " \
                      "people with lung disease, older adults and children are at a greater risk from exposure " \
                      "to ozone, whereas persons with heart and lung disease, older adults and children are " \
                      "at greater risk from the presence of particles in the air."
        category_color = "usg"
    elif air_now_api_result[0]['Category']['Name'] == "Unhealthy":
        description = "(151 - 200) Everyone may begin to experience health effects; members of " \
                      "sensitive groups may experience more serious health effects."
        category_color = "unhealthy"
    elif air_now_api_result[0]['Category']['Name'] == "Very Unhealthy":
        description = "(201 - 300) Health alert: everyone may experience more serious health effects."
        category_color = "veryunhealthy"
    elif air_now_api_result[0]['Category']['Name'] == "Hazardous":
        description = "(301 - 500) Health warnings of emergency conditions. The entire population is " \
                      "more likely to be affected."
        category_color = "hazardous"
    return description, category_color


def set_overige_waarden(air_now_api_result):
    category_name = air_now_api_result[0]['Category']['Name']
    aqi = air_now_api_result[0]['AQI']
    area = air_now_api_result[0]['ReportingArea']
    return category_name, aqi, area


def set_overige_waarden_nl(air_now_api_result):
    city = air_now_api_result['liveweer'][0]['plaats']
    temp = air_now_api_result['liveweer'][0]['temp']
    samenv = air_now_api_result['liveweer'][0]['samenv']
    windr = air_now_api_result['liveweer'][0]['windr']
    zicht = air_now_api_result['liveweer'][0]['zicht']
    verw = air_now_api_result['liveweer'][0]['verw']
    sunder = air_now_api_result['liveweer'][0]['sunder']
    if samenv == 'Licht bewolkt':
        afbeelding = 'https://www.leukvoorkids.nl/wp-content/uploads/het-weer-0008-half-bewolkt.gif'

    elif samenv == 'Zonnig':
        afbeelding = 'https://rtvhollandsmidden.nl/wp-content/uploads/2017/05/zonnig.jpg'

    else: afbeelding ='https://signland.nl/wp-content/uploads/2017/09/ss-lts-vraagteken.jpg'

    return city, temp, samenv, windr, zicht, verw, sunder, afbeelding

