# This is my views.py file
from django.urls import path
from . import views

urlpatterns = [
    path('', views.buitenland, name="home"),
    path('about.html', views.about, name="about"),
    path('help.html', views.help, name="help"),
    path('nederland.html', views.nederland, name="nederland"),
]
