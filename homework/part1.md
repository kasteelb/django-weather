### Werken aan de eerste app!
In de online cursus hebben jullie wat uitleg gekregen over hoe deze app is gemaakt. Hij is op dit moment *iets* anders dan jullie hebben gezien: in de code stond veel herhaling (in lookup/views.py), die heb ik proberen weg te halen.

We gaan (minstens) twee weken aan deze app werken. De eerste week zijn de opdrachten herhalingen of kleine veranderingen. De tweede week gaan we er proberen wat dingen aan toe te voegen!

Note: tot nu toe hebben we alleen wat geoefend met basis python. Nu komt er waarschijnlijk een heleboel op jullie af: html, het doen van requests, allemaal code verspreid over verschillende files. Dit is een recept voor migraine; dus trek vooral aan de bel als het te veel is of te snel gaat.

#### Opdrachten
1. Clone het project en <b>*maak je eigen branch aan*</b> (Git). Vraag ook toestemming om voor dit project code te mogen pushen. Steeds als je een stukje werkende code hebt, dan is het de bedoeling dit te pushen, zodat het ook online komt te staan.


2. (kijken en runnen) Probeer in een python-console de code die in `views.py` te begrijpen door stapsgewijs wat stukjes code te runnen. Probeer bijvoorbeeld antwoord te krijgen op de volgende vragen<br>
    - Wat doet de functie url_for_zipcode? Run de functie voor een willekeurige zipcode.
    - Wat gebeurt er als je in de browser naar die url gaat?
    - Voer de regel `api_request = requests.get(url_for_zipcode(zipcode))` uit, voor de zipcode van New York. <br>
    Met het commando `api_request.__dict__` kun je zien wat voor een informatie we allemaal terugkrijgen. <br>
      Met het commando `json.loads(api_request.content)` zie je de *content* waar je daadwerkelijk om gevraagd hebt.
    - Hoe ziet de content er uit als je een foute zipcode geeft, bijvoorbeeld de `zipcode = 'fout'`?
    - Hoe gaat de app om met die gevallen?



3. Maak in de navbar een nieuwe knop `Help`. Als daar op geklikt wordt, moet de gebruiker een pagina te zien met uitleg hoe de app werkt.


4. Als de weer-api ineens anders gaat werken, dan gaat onze code waarschijnlijk kapot: niks aan te doen. <br>
De schade blijft beperkt als we dan op maar 1 plek onze code hoeven aan te passen. <br>
   Op dit moment wordt de data uit de weer-api in `home.html` uitgepakt: bijvoorbeeld door een commando `api_content.0.ReportingArea`. We pakken de data ook uit in `helpers.py`, door commando's als `air_now_api_result[0]['Category']['Name']`. <br>
   De opdracht is om al het uitpakken in de helper-functie te doen. Die helper functie moet dan meer variabelen teruggeven, namelijk alle variabelen die we in `home.html` nodig hebben. Zorg dan dat de uitgepakte variabelen ook daadwerkelijk in `home.html` gebruikt worden. <br>
   Mocht de weer-api die we gebruiken nu wijzigen, dan hoeven we alleen code in `helpers.py` aan te passen!
   
   
